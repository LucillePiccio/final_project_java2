public enum Colours{
	RED{
		public String toString(){
			return "Red";
		}
	},
	BLUE{
		public String toString(){
			return "Blue";
		}
	},
	GREEN{
		public String toString(){
			return "Green";
		}
	},
	YELLOW{
		public String toString(){
			return "Yellow";
		}
	}
}