import java.util.*;
public class Uno{
	public static void main(String[] args){
	//calling objects
	Scanner sc = new Scanner(System.in);
	Player playerObj = new Player();
	Player[] players = new Player[5];
	for(int i = 0; i < players.length; i++){
		players[i] = new Player();
	}			
	CardPile cardObj = new CardPile();
	DrawPile deckObj = new DrawPile();
	//creating gameDeck
	DrawPile gameDeck = deckObj;
	gameDeck.shuffleDeck();
	Gameplay gameP = new Gameplay(players,gameDeck);
	
	//player counter
	int turn = 1;
	//will go through all 4 players
	for(int i = 0; i < players.length; i++){
		for(int j = 0; j < 7 ; j++){
		gameDeck.drawTopCard();
		//each player will be handed 7 cards
		players[i].takeCard(gameDeck.drawTopCard());
		}	
	}
	//game starts here
	printWelcome();
	boolean isGameOver = false;
	
	Card faceUpCard = gameDeck.drawTopCard();
	while(!(isGameOver)){
		//isGameOver = gameP.hasWon(players);
		System.out.println("FACE UP CARD: " + "\n" + faceUpCard);
		if(turn > 4){
		turn = 1;
		}
	
	System.out.println("PLAYER" + turn + " DECK OF CARDS:" + "\n" + players[turn].toString() + printNextLine());
	
	boolean input = false;
	int index;
	while(!input){
		try{
			if(gameP.isPlayable(faceUpCard, turn)){
				System.out.println("Place a valid card, start counting from position 0");
				index = sc.nextInt();
				Card playerCard = players[turn].placeCard(index);
				
				if(gameP.isSkipTurn(playerCard) && gameP.isColourValid(playerCard, faceUpCard)){
					playsTurn(players, turn, playerCard, faceUpCard, index);
					faceUpCard = playerCard;
					input = true;
					//skips 2 turns because skipcard was used
					turn+=2;
					}
				else if(gameP.isNumberDupe(playerCard, faceUpCard)){
					playsTurn(players, turn, playerCard, faceUpCard, index);
					faceUpCard = playerCard;
					input = true;
					turn++;
					}
				else if(gameP.isColourValid(playerCard, faceUpCard)){
					playsTurn(players, turn, playerCard, faceUpCard, index);
					faceUpCard = playerCard;
					//input becomes true so that we move on to the next place once they picked up the card
					input = true;
					turn++;
					}
				}
				else{
				System.out.println("no playable cards...pick up a card");
				players[turn].takeCard(gameDeck.drawTopCard());
				System.out.println("PLAYER" + turn + " DECK OF CARDS:" + "\n" + players[turn].toString() + printNextLine());
				input = true;
				turn++;
				}
			}
			catch(ArrayIndexOutOfBoundsException error1){
			System.out.println("Card position should be between 0 and the number of current cards you have!");
				}
			catch(InputMismatchException error2){		
			System.out.println("Card position should be a whole number");
			sc.next();
				}
			}
			if(gameP.hasWon(players)){
				isGameOver = true;
			}
		}
		//because previous code always goes to the next player, to bring it back to the previous player, we do turn -1
		turn--;
		System.out.println("PLAYER" + turn + " WON!");
	}
	
	//helper methods
	//updates the face up card based on the player's draw card
	private static void playsTurn(Player[] players, int turn, Card playerCard, Card faceUpCard, int index){
		System.out.println("PLAYER" + turn + " PLAYED " + playerCard);
		players[turn].removeCard(index);
		faceUpCard = playerCard;
		System.out.println("CURRENT FACE UP CARD" + "\n" + faceUpCard + printNextLine());
	}
	
	//simply makes the next print statement display onto the next line. Helps with readability.
	private static String printNextLine(){
		String next = "\n";
		return next;
	}
	
	//Prints welcome message and uno rules
	private static void printWelcome(){
		System.out.println("Hello! Welcome to the 420-210 DW Programming 2 Winter 2023 Uno Game!" + printNextLine());
		System.out.println("Here are the rules..." + printNextLine());
		System.out.println("1. This is a 4 player game.");
		System.out.println("2. A player can only place a card if:");
		System.out.println("		- the card is of the same colour as the current face up card");
		System.out.println("		- the card has the same number of the current face up card");
		System.out.println("		- a skip card can only be placed if it is of the same colour as the current face up card");
		System.out.println("		- the skip turn feature is only activated when the card is also of the same colour");
		System.out.println("3. If the player does not have any playable card, they will automatically be given a card and skip their turn");
		System.out.println("4. To win the game, you must be the first player to finish their deck of cards" + printNextLine());
	}
}