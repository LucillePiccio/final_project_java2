import java.util.Random;
public class DrawPile{
	private CardPile drawPile;

	//constructor
	public DrawPile(){
	
		this.drawPile = drawPile;
		
		CardPile cardP = new CardPile();
		this.drawPile = cardP;
		//assigning drawPile dynamic array of CardPile type to create the drawPile
		for(Colours c: Colours.values()){
			for(Numbers n: Numbers.values()){
				for(int i = 0; i < 2; i++){
				Card card = new Card(c,n);
				this.drawPile.addToPile(card);
				}
			}
		}
	}
	
	//get method
	public int getDrawPileLength(){
		return this.drawPile.getLengthOfValues();
	}
	
	//set method
	public void setDrawPile(int index, Card card){
		this.drawPile.setCard(index, card);
	}
	
	//shuffleDeck method that swaps the cards based on a randomly generated index 
	public void shuffleDeck(){
		Random r = new Random();
		for(int i = 0; i < this.drawPile.getLengthOfValues(); i++){
		int rand = r.nextInt(this.drawPile.getLengthOfValues());
		this.drawPile.setCard(rand, this.drawPile.getCardAtIndex(i));
		}
	}
	
	//insertCard method receives a card and inserts into drawPile and increases its length
	public void insertCard(Card card){
		this.drawPile.addToPile(card);
	}
	
	//drawTopCard returns the first card of the deck
	public Card drawTopCard(){
		Card c = this.drawPile.getCardAtIndex(this.drawPile.getLengthOfValues() - 1);
		this.drawPile.removeFromPile(this.drawPile.getLengthOfValues() - 1);
		return c;
	}
	
	//toString method 
	public String toString(){
		String discardStr = "";
		for(int i = 0; i < drawPile.getLengthOfValues(); i++){
			discardStr += this.drawPile.getCardAtIndex(i);
		}
		return " " + discardStr + " ";
	}
}
	
