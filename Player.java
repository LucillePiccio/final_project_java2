public class Player{
	private CardPile playerDeck;
	
	//constructor
	public Player(){
		this.playerDeck = playerDeck;
		CardPile cardP = new CardPile();
		this.playerDeck = cardP;
	}

	//get method
	public CardPile getplayerDeck(){
		return this.playerDeck;
	}
	
	public int getPlayerDeckLength(){
		return this.playerDeck.getLengthOfValues();
	}
	
	//set method
	public void setplayerDeck(Card card){
	for(int i = 0; i < this.playerDeck.getLengthOfValues(); i++){
		this.playerDeck.setCard(this.playerDeck.getLengthOfValues(), card);
		}
	}
	
	//removeCard method takes out a card from the player deck
	public void removeCard(int index){
		this.playerDeck.removeFromPile(index);
	}
	
	//takeCard method takes a card from the main deck and return nothing(similar to AddToPile), will add new card to player's deck
	public void takeCard(Card card){
		this.playerDeck.addToPile(card);
	}
	
	//placeCard method where the player will pick a card from their deck and place it at the discardPile
	public Card placeCard(int index){
		return this.playerDeck.getCardAtIndex(index);
	}
	
	//verifies if the player deck has any playable card to the current faceUpCard
	public boolean doesContain(Card card){
		boolean isPlayable = false;
		for(int i = 0; i < this.playerDeck.getLengthOfValues(); i++){
		if(playerDeck.getCardAtIndex(i).getColour().equals(card.getColour()) || playerDeck.getCardAtIndex(i).getValue().equals(card.getValue())){
			return true;
			}
		}
		return isPlayable;
	}
	public String toString(){
		String player = "";
		for( int i = 0; i < this.playerDeck.getLengthOfValues(); i++){
			player += this.playerDeck.getCardAtIndex(i);
		}
	return "{" + player + "}";
	}
}
