public class Tester{
	public static void main (String[]args){
	//testing single card class
	System.out.println("testing Card class here");
	Card singleCard1 = new Card(Colours.RED,Numbers.ONE);
	System.out.println(singleCard1);
	Card singleCard2 = new Card(Colours.BLUE,Numbers.TWO);
	System.out.println(singleCard2);
	Card singleCard3 = new Card(Colours.GREEN,Numbers.THREE);
	System.out.println(singleCard3);
	
	//testing CardPile class
	System.out.println("testing CardPile class here");
	
	//testing addToPile method
	CardPile mainP = new CardPile();
	mainP.addToPile(singleCard1);
	mainP.addToPile(singleCard3);
	mainP.addToPile(singleCard3);
	System.out.println("Current array length is: " + mainP.getLengthOfValues());
	System.out.println("testing getCardAtIndex method: " + mainP.getCardAtIndex(2));
	
	//testing drawPile class
	DrawPile drawP = new DrawPile();
	System.out.println("before shuffling");
	System.out.println(drawP.toString());
	System.out.println("after shuffling"); //omg it works thank you
	drawP.shuffleDeck();
	System.out.println(drawP.toString());
	
	//testing PLayer.java 
	System.out.println("testing Player class here");
	Player[] p = new Player[7];
	Player pl = new Player();
	for(int i = 0; i < p.length; i++){
		pl.insertToPile(singleCard1);
	}
	System.out.println("the player currently has " + pl.getPlayerDeckLength() + " cards");
	System.out.println(pl.toString());
	System.out.println("now I place a card");
	System.out.println(pl.placeCard(0)); //omg it also works thank you
	System.out.println("the player currently has " + pl.getPlayerDeckLength() + " cards");
	System.out.println(pl.toString());
	System.out.println("the player took a card");
	pl.takeCard(singleCard3);
	System.out.println("the player currently has " + pl.getPlayerDeckLength() + " cards");
	System.out.println(pl.toString());
	
	//testing gameplay.java
	Gameplay game = new Gameplay();
	}
}