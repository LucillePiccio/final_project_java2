public class Card{
	private Colours colour;
	private Numbers value;
	
	//constructor
	public Card(Colours colour, Numbers value){
		this.colour = colour;
		this.value = value;
	}
	
	//get methods
	public Colours getColour(){
		return this.colour;
	}
	
	public Numbers getValue(){
		return this.value;
	}
	
	//set methods
	public void setColour(Colours colour){
		this.colour = colour;
	}
	
	public void setValue(Numbers value){
		this.value = value;
	}
	
	//toString method
	public String toString(){
	return "[" + this.colour + "," + this.value + "]";
	}
}