//my dynamic card array class
import java.util.Random;
	public class CardPile{
	private Card[] card;
	private int lengthOfValues;
	
	//constructor
	public CardPile(){
		this.card = new Card[1000];
		this.lengthOfValues = 0;
	}
	Random r = new Random();
	
	//get methods
	public int getLengthOfValues(){
		return this.lengthOfValues;
	}
	
	public Card[] getCard(){
		return this.card ;
	}
	
	//set methods
	public void setCard(int index, Card card){
		if(index >= this.lengthOfValues){
		throw new ArrayIndexOutOfBoundsException("Invalid index! Index should be between 0 and card array length");
		}
		this.card[index] = card;
	}
	
	//getCardAtIndex method returns a card based on the index input
	public Card getCardAtIndex(int index){
		if(index >= this.lengthOfValues){
			throw new ArrayIndexOutOfBoundsException("Invalid index! Index should be between 0- card array length");
		}
		return this.card[index];
	}
		
	//overwrite method that takes a card and an index
	public void overwriteCard(Card card, int index){
		if(index >= this.lengthOfValues){
			throw new ArrayIndexOutOfBoundsException("index should be between 0 and the current array length");
		}
		this.card[index] = card;
	}
	
	//custom methods
	//removeFromPile method that removed a single card from the pile
	public void removeFromPile(int index){
		if(index >= this.lengthOfValues){
			throw new ArrayIndexOutOfBoundsException("index should be between 0 and the current array length");
		}
		for(int i = index; i < this.lengthOfValues; i++){
			this.card[i] = this.card[i+1];
		}
		this.lengthOfValues--;
	}
	
	//addToPile method that adds a single card into the pile of cards
	public void addToPile(Card card){
		if(this.lengthOfValues > this.card.length){
			duplicateSize();
		}
		this.card[this.lengthOfValues] = card;
		this.lengthOfValues++;
	}
	
	//duplicateSize method to increase dynamic array if needed
	private void duplicateSize(){
	 Card[] bigger = new Card[card.length*2];
	 for(int i = 0; i < card.length; i++){
		 bigger[i] = card[i];
		}
		card = bigger;
	}
	
	//toString method
	public String toString(){
		String cardArrStr = "";
		for(int i = 0; i < this.card.length; i++){
			cardArrStr += this.card[i] + "\n";
		}
		return cardArrStr;
	}
	
}