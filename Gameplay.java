public class Gameplay{
	private Player[] players;
	private DrawPile deck;
	
	//constructor
	public Gameplay(Player[] players, DrawPile deck){
		this.players = players;
		this.deck = deck;
	}
	
	//verifies if colour is the same as face up card
	public boolean isColourValid(Card card1, Card card2){
		boolean isValid = false;
		if(card1.getColour().equals(card2.getColour())){
			isValid = true;
		}
		return isValid;
	}
	
	//verifies if number of draw card and face up card number is the same
	public boolean isNumberDupe(Card card1, Card card2){
		boolean isDupe = false;
		if(card1.getValue().equals(card2.getValue())){
			isDupe = true;
		}
		return isDupe;
	}
	
	//verifies if the card placed by the player is a special skipTurn card
	public boolean isSkipTurn(Card card1){
		boolean isSkip = false;
		if(card1.getValue().equals(Numbers.SKIPTURN)){
			isSkip = true;
		}
		return isSkip;
	}
	
	//verifies if the current player has any playable card for the current face up card
	public boolean isPlayable(Card card, int currentPlayer){ 
		boolean doesHave = false;
		if(this.players[currentPlayer].doesContain(card)){
			doesHave = true;
		}
		return doesHave;
	}
	//gameloop condition, verifies if a player has an empty deck of cards
	public boolean hasWon(Player[] players){
		boolean isOver = false;
		for(int i = 0; i < this.players.length; i++){
			if(players[i].getPlayerDeckLength() == 0){
				isOver = true;
			}
		}
		return isOver;
	}
}